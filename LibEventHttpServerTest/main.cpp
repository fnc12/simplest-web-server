#include <memory>
#include <cstdint>
#include <iostream>
#include <evhttp.h>
#include <cstdint>

using std::cout;
using std::endl;

int main()
{
    if (!event_init())
    {
        std::cerr << "Failed to init libevent." << std::endl;
        return -1;
    }
    char const srvAddress[] = "127.0.0.1";
    std::uint16_t srvPort = 80;
    std::unique_ptr<evhttp, decltype(&evhttp_free)> server(evhttp_start(srvAddress, srvPort), &evhttp_free);
    if (!server)
    {
        std::cerr << "Failed to init http server." << std::endl;
        return -1;
    }
    auto onReq = [] (evhttp_request *req, void *ototo)
    {
        auto *outBuf = evhttp_request_get_output_buffer(req);
        if (!outBuf)
            return;
        auto uri = evhttp_request_get_uri(req);
        cout << "uri = " << (uri ? uri : "null") << endl;
        auto header = evhttp_request_get_input_headers(req);
        struct evkeyval* kv = header->tqh_first;
        while (kv) {
            printf("%s: %s\n", kv->key, kv->value);
            kv = kv->next.tqe_next;
        }
        evbuffer_add_printf(outBuf, "<html><body><center><h1>Hello Wotld!</h1></center></body></html>");
        evhttp_send_reply(req, HTTP_OK, "", outBuf);
        cout << "Response is sent!" << endl;
    };
    evhttp_set_gencb(server.get(), onReq, nullptr);
    if (event_dispatch() == -1)
    {
        std::cerr << "Failed to run messahe loop." << std::endl;
        return -1;
    }
    return 0;
}
